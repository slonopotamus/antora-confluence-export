# Antora Confluence Export

An [Antora](https://antora.org/) extension that publishes contents to [Confluence](https://www.atlassian.com/software/confluence).
