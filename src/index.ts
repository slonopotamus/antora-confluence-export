import pino from "pino";

export class AntoraConfluenceExportExtension {
  private logger: pino.Logger;

  static register() {
    return new AntoraConfluenceExportExtension(this);
  }

  constructor(context: any) {
    const { name } = require("../package.json");
    this.logger = context.getLogger(name);
    context.on("beforePublish", this.onBeforePublish.bind(this));
  }

  onBeforePublish({ siteCatalog }: any) {
  }
}

module.exports = AntoraConfluenceExportExtension;
