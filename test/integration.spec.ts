import { describe, test } from "@jest/globals";
import path = require("path");

const generateSite = require("@antora/site-generator");

const workDir = path.join(__dirname, "work");
const outputDir = path.join(workDir, "public");
const cacheDir = path.join(workDir, ".antora", "cache");
const fixturesDir = path.join(__dirname, "fixtures");

describe("generateSite()", () => {

  const defaultPlaybookFile = path.join(
    fixturesDir,
    "docs-site/antora-playbook.yml"
  );

  test("should generate site", async () => {
    await generateSite(
      [
        "--playbook",
        defaultPlaybookFile,
        "--to-dir",
        outputDir,
        "--cache-dir",
        cacheDir,
        "--quiet"
      ],
      {}
    );
  });
});
